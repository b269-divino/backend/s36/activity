// Set up the dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");


//server set up
const app = express();
const port = 3001;

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//database connection
mongoose.connect ("mongodb+srv://joeydivino:admin123@zuitt-bootcamp.tkfjv2z.mongodb.net/s36?retryWrites=true&w=majority",
	{
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);
mongoose.connection.once("open", () => console.log(`Now connected to port ${port}.`));

// Add task route
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to ${port}.`));








